$(document).ready(function () {
    var btntoggled = false;
    $('.link-list').hide();
    $('#btn-sadrzaj').click(function () {
      $('.link-list').toggle();
      if (btntoggled == false) {
        $('#btn-sadrzaj').html('<i class="fas fas fa-angle-up" style="font-size:20px">&nbsp;</i>Sakri sadrzaj');
        btntoggled = true;
      }
      else {
        $('#btn-sadrzaj').html('<i class="fas fas fa-angle-down" style="font-size:20px">&nbsp;</i>Sadrzaj');
        btntoggled = false;
      }
    });
  });