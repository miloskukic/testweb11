$(document).ready(function(){
    $("#speach-to-text, #text-to-speach, #api-analytics,#compute-engine").hide();
    $("#speach-to-text").show();     

    $("#pt1-1").click(function(){
      $("#speach-to-text").toggle(100);
      $("#text-to-speach,#api-analytics,#compute-engine").hide();
    })
    $("#pt1-2").click(function(){
      $("#text-to-speach").toggle(100);
      $("#speach-to-text,#api-analytics,#compute-engine").hide();
    })
    $("#pt2-1").click(function(){
      $("#api-analytics").toggle(100);
      $("#speach-to-text,#text-to-speach,#compute-engine").hide();
    })
    $("#pt3-1").click(function(){
      $("#compute-engine").toggle(100);
      $("#text-to-speach,#speach-to-text,#text-to-speach,#api-analytics").hide();
    })

    var btntoggled = false;
    $('.link-list').hide();
    $('#btn-sadrzaj').click(function () {
      $('.link-list').toggle();
      if (btntoggled == false) {
        $('#btn-sadrzaj').html('<i class="fas fas fa-angle-up" style="font-size:20px">&nbsp;</i>Sakri pakete');
        btntoggled = true;
      }
      else {
        $('#btn-sadrzaj').html('<i class="fas fas fa-angle-down" style="font-size:20px">&nbsp;</i>Paketi');
        btntoggled = false;
      }
    });

    //btn-valuta
    var step=0;
    $('#btn-valuta').click(function(){
        switch(step){
            case 1: $('#btn-valuta').html('<i class="fas fa-euro-sign" style="font-size:20px">&nbsp;</i>Evro');step++;alert('Evro');break;
            case 0: $('#btn-valuta').html('<i class="fas fa-dollar-sign" style="font-size:20px">&nbsp;</i>Dolar');step++;alert('Dolar');break;
            case 2: $('#btn-valuta').html('<i class="fas fa-money-bill-alt" style="font-size:20px">&nbsp;</i>Dinar');step++;alert('Dinar');break;
            default: step=0;$('#btn-valuta').html('<i class="fas fa-euro-sign" style="font-size:20px">&nbsp;</i>Evro');alert('Evro');break;
        }
    })
   

  })