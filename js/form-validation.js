$(document).ready(function () {
   
    
    
    $("#error,#error2, #error3,#success").hide();


$("#imeIprezime").focusout(function (e) { 
    var poruka=$("#error");
    var imeIprezime=$('#imeIprezime').val();
    
    if((imeIprezime.length < 4)|| (imeIprezime.length == 0)){
        $(poruka).show();
        $(poruka).html("Broj karaktera mora biti veći od 4");
    }else{
        $(poruka).hide();
    }
    
});

$("#email").focusout(function(e){
    var email=$("#email").val();
    var poruka=$("#error2");

    if(email.length < 4){
      $(poruka).show();
        $(poruka).html("Broj karaktera mora biti veći od 4");
    }else{
        $(poruka).hide();
    }
    if((email.indexOf("@") !== -1)&& (email.indexOf(".com") !== -1)){
        $(poruka).hide();
    }else{
       
        $(poruka).show();
        $(poruka).html("Email adresa nije ispravna");
    }


})

$("#poruka").focusout(function(){
    var tekst=$("#poruka").val();
    var poruka=$("#error3");

    if(tekst.length > 120){
        $(poruka).show();
        $(poruka).html("Nadmašili ste broj dozvoljenih karaktera!(120)")
    }else{
        $(poruka).hide();
    }

    if(tekst.length === 0){
        $(poruka).show();
        $(poruka).html("Poruka ne može ostati prazna");
    }else{
        $(poruka).hide();
    }

$("#submit").click(function (e) { 
    e.preventDefault();
    if($("#error,#error2,#error3").is(':visible')){
        
        alert("Ne možete poslati komentar dok greške ne budu otklonjene")

    }else{
        $("#success").show();
        $("#success").html('<i class="fas fa-check"></i>&nbsp;&nbsp;Poruka uspesno poslata');
        setTimeout(function(){$("#success").hide();},8000)
        $('#imeIprezime').val(" ");
        $('#email').val(" ");
        $('#poruka').val(" ");
    }

});

  
})





    
});

